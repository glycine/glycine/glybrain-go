package utils

import (
	"context"
	"github.com/docker/docker/api/types"
	cont "github.com/docker/docker/api/types/container"
	"github.com/docker/docker/client"
	"github.com/docker/go-connections/nat"
	error2 "glybrain-go/error"
	"strconv"
	"time"
)

var (
	cli, err          = client.NewClientWithOpts(client.WithVersion("1.39")) //新建客户端
	ctx               = context.Background()                                 //上下文
	containersMap     = make(map[container]string)                           //name-id
	ContainerNKDir    = "/srv/nukkit"
	HostTempBaseDir   = "./temp/base"
	HostTempUniqueDir = "./temp/unique"
)

type container string

type Container interface {
	Name() string
	ListImage() error
	Create(port int) error
	Start() error
	Stop() error
	Remove() error
	GetCli() *client.Client
}

//新建一个容器，传入参数为容器名
func NewContainer(name string) Container {
	cont := container(name)
	return &cont
}

func init() {
	if err != nil {
		panic(err)
	}
}

func (c container) Name() string {
	return string(c)
}

// 列出镜像
func (c container) ListImage() error {
	images, err := cli.ImageList(context.Background(), types.ImageListOptions{})
	if err == nil {
		for _, image := range images {
			GetLogger().Info("=====[docker images]=====\n")
			GetLogger().Info(image)
		}
	}
	return err
}

// 创建容器，传入想绑定到宿主机上的端口号，返回一个错误类型
func (c container) Create(hostPort int) error {
	if _, ok := containersMap[c]; ok {
		return error2.ContainerErr(error2.CreatedContainerErr)
	}
	exports := make(nat.PortSet, 10)
	port, err := nat.NewPort("udp", "19132") //设定容器需要暴露的端口，为19132
	if err != nil {
		return err
	}
	exports[port] = struct{}{}
	config := &cont.Config{Image: "ixilon/nukkit", ExposedPorts: exports}
	portBind := nat.PortBinding{HostPort: strconv.Itoa(hostPort)} //绑定到宿主机的端口，由参数传入
	portMap := make(nat.PortMap, 0)
	tmp := make([]nat.PortBinding, 0, 1)
	tmp = append(tmp, portBind)
	portMap[port] = tmp
	hostConfig := &cont.HostConfig{PortBindings: portMap}

	//命名格式应为 clienttype+num 如 lobby1
	body, err := cli.ContainerCreate(ctx, config, hostConfig, nil, c.Name())
	if err == nil {
		GetLogger().Debug("[docker]", "容器", c, "创建成功")
		containersMap[c] = body.ID
	}
	return err
}

// 启动, 请在调用本方法的业务逻辑处写入 defer c.Stop
func (c container) Start() error {
	if _, ok := containersMap[c]; !ok {
		return error2.ContainerErr(error2.UncreatedContainerErr)
	}
	err := cli.ContainerStart(ctx, containersMap[c], types.ContainerStartOptions{})
	if err == nil {
		GetLogger().Debug("[docker]", "容器", c, "启动成功")
	}
	return err
}

// 停止
func (c container) Stop() error {
	if _, ok := containersMap[c]; !ok {
		return error2.ContainerErr(error2.UncreatedContainerErr)
	}
	timeout := time.Second * 10 //关闭超时
	err := cli.ContainerStop(ctx, containersMap[c], &timeout)
	if err == nil {
		GetLogger().Debug("[docker]", "容器", c, "已经被停止")
	}
	return err
}

// 删除
func (c container) Remove() error {
	if _, ok := containersMap[c]; !ok {
		return error2.ContainerErr(error2.UncreatedContainerErr)
	}
	err := cli.ContainerRemove(ctx, containersMap[c], types.ContainerRemoveOptions{})
	if err == nil {
		delete(containersMap, c) //从map中移除已被remove的容器
		GetLogger().Debug("[docker]", "容器", c, "已经移除\n")
	}
	return err
}

//获取docker client
func (c *container) GetCli() *client.Client {
	return cli
}

//获取容器map，格式为 c-id
func GetContainersMap() *map[container]string {
	return &containersMap
}

//展示容器们
func ListContainer() {
	GetLogger().Info("=====[docker containers]=====")

	if len(containersMap) == 0 {
		GetLogger().Info("EMPTY")
	} else {
		for co, id := range containersMap {
			GetLogger().Info("[NAME]  ", co, "    [ID]  ", id)
		}
	}
}
