package subject

import "glybrain-go/event/observer"

type Subject interface {
	Registry()
	DeRegistry()

	Notify()
	Update(context string)
}

type subject struct {
	observers map[observer.Observer]struct{}
}

func NewSubject() *subject {
	return &subject{
		observers: make(map[observer.Observer]struct{}),
	}
}

func (s *subject) Attach(o observer.Observer) {
	s.observers[o] = struct{}{}
}

func (s *subject) DeAttach(o observer.Observer) {
	delete(s.observers, o)
}

//通知所有的观察者,可以在这里区别是否发送东西给摸一个观察者。
//这里的通知策略可以给目标添加一个interface，然后不同的通知策略实现不同的类赋值给这个interface就可以了
//当然，也可以在观察者的update函数中忽略不想处理的消息，只处理感兴趣的消息就好了。
func (s *subject) Notify() {
	for _, o := range s.observers {
		//把整个观察者都发过去了，也可以只发一部分消息，让接受者决定要不要继续拿更多东西
		o.Update(s)
	}
}

func (s *subject) UpdateContext(context string) {
	s.context = context
	s.Notify()
}
