package observer

import (
	event2 "glybrain-go/event"
	"glybrain-go/utils"
)

type Observer interface {
	Update(*event2.Event)
}

type observer struct {
	Id int
}

func (o *observer) Update(e *event2.Event) {
	utils.GetLogger().Info("observer [%d] recieved msg: %s.\n", o.Id, (*e).GetContext())
}

//var e Event  = NewCommandEvent([]string{})
