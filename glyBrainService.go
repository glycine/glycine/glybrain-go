package glybrain_go

import (
	"fmt"
	console2 "glybrain-go/console"
)

var Srv *glyBrainService

type glyBrainService struct {
	instance  *glyBrainService
	isRunning bool
	path      string
	console   *console2.Console
}

func NewGlyBrainService(p string) {
	Srv = &glyBrainService{
		Srv,
		true,
		p,
		console2.NewConsole(),
	}
}

//應最後啟動
func (srv *glyBrainService) Start() {
	srv.console.Start()
}

func (srv *glyBrainService) Stop() {
	srv.console.Stop()
}

/**
implement the event listener interface
*/

func Test() {
	fmt.Println("nmsl")
}
