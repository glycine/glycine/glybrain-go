package console

type GlyConsole interface {
	IsRunning() bool
	ReadCmd()
	RunCmd(cmd string)
	Shutdown()
	Start()
}
