package console

import (
	"bufio"
	"container/list"
	"fmt"
	"glybrain-go/utils"
	"os"
	"strings"
)

type Console interface {
	Start()
	IsRunning() bool
}

type console struct {
	reader *bufio.Reader
	cmds   *list.List
	active bool
}

func NewConsole() *console {
	return &console{bufio.NewReader(os.Stdin), list.New(), nil}
}

func (c *console) Start() {
	c.cmds.Init()
	c.active = true
	for c.active {
		fmt.Print("B>> ")
		c.ReadCmd()

		for e := c.cmds.Back(); e != nil; e = e.Prev() {
			OnCommand(e.Value.([]string))
			c.cmds.Remove(e)
		}
	}
}

func (c *console) IsRunning() bool {
	return false
}

func (c *console) ReadCmd() {
	cmd, _ := c.reader.ReadString('\n')
	cmd = strings.Replace(cmd, "\n", "", -1)
	cmdSlice := strings.Split(cmd, " ")
	c.cmds.PushFront(cmdSlice)
}

func (c *console) RunCmd(cmd string) error {
	utils.GetLogger().Info("run" + cmd)
	return nil
}

func (c *console) Stop() {
	c.active = false
}
