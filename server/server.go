package server

import "glybrain-go/utils"

var (
	serversMap           = make(map[string]server) //containerID-server
	unregisterServersMap = make(map[string]server)

	checkerMap = make(map[string]utils.TimeoutChecker)
)

type server struct {
	cfg       IConfig
	isRunning bool
	container utils.Container
}

func NewServer(cfg IConfig, c utils.Container) *server {
	return &server{cfg, nil, c}
}
