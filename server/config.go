package server

import "glybrain-go/utils"

type config struct {
	port int
	name string
	ct   utils.ClientType
	para string
}

type IConfig interface {
	GetPort() int
	GetPara() string
	GetName() string
	GetClientType() utils.ClientType
	ToStr() string
}

func NewServerCfg(port int, name string, ct utils.ClientType, para string) *config {
	return &config{port, name, ct, para}
}

func (cfg config) GetPort() int {
	return cfg.port
}

func (cfg config) GetPara() string {
	return cfg.para
}

func (cfg config) GetName() string {
	return cfg.name
}

func (cfg config) GetClientType() utils.ClientType {
	return cfg.ct
}

func (cfg config) ToStr() string {
	return string(cfg.port) + " | " + cfg.name + " | " + string(cfg.ct) + " | " + cfg.para
}
